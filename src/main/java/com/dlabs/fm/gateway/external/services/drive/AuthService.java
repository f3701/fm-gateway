package com.dlabs.fm.gateway.external.services.drive;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AuthService {

//    @Autowired
//    private RestTemplate restTemplate;
//
//    @Value("${service.auth.urlbase}")
//    private String URL_BASE_AUTH;
//
//
//    public void sendLogin(String username, String password) {
//        ResponseDto response = this.restTemplate.postForEntity(URL_BASE_AUTH + "/login").getBody();
//        log.info(Objects.requireNonNull(response).toString());
//    }

    public void validatePermissions(String endpoint, String method, String token) {
        throw new RuntimeException("Unauthorized.");
    }
}
