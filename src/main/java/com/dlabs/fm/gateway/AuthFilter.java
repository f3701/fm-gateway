package com.dlabs.fm.gateway;

import com.dlabs.fm.gateway.external.services.drive.AuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class AuthFilter implements GlobalFilter {

    @Autowired
    private AuthService authService;

    private static final String LOGIN_ENDPOINT = "/auth/api/v1/login";
    private static final String POST_METHOD = "POST";

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        
        log.info(exchange.getRequest().getURI().getPath());
        log.info(exchange.getRequest().getMethodValue());
        
        String endpoint = exchange.getRequest().getURI().getPath();
        String method = exchange.getRequest().getMethodValue();
        String token = "";

        if (!endpoint.equals(LOGIN_ENDPOINT)) {
            // Call authorize
            // this.authService.validatePermissions(endpoint, method, token);
        }

        return chain.filter(exchange);
    }

//    @Override
//    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
//        System.out.println(">>> Custom filter.");
//    }
}
